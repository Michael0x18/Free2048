extends Node2D
class_name Tile

var xindex:int
var yindex:int

var xtarget:int
var ytarget:int

var val:int
#if tile was created this cycle (used to prevent multiple merges when sliding, e.g. 844 -> 16 is wrong,
#should be 88)
var is_new:bool
var destroy_on_finish = false

func _init():
	destroy_on_finish = false
	self.z_index = 10

#Sync the label text to the value
func update_tile():
	$Sprite2D/Label.text = str(val)

func _process(delta):
	if(global_position.x != xtarget):
		global_position.x = lerp(global_position.x, xtarget, 20 * delta)
	if(global_position.y != ytarget):
		global_position.y = lerp(global_position.y, ytarget, 20 * delta)
	if(abs(global_position.x-xtarget)<1):
		global_position.x = xtarget
	if(abs(global_position.y-ytarget)<1):
		global_position.y = ytarget
	if(destroy_on_finish && global_position.x == xtarget && global_position.y == ytarget):
		get_parent().remove_child(self)
		self.queue_free()

func trigger_destroy():
	self.z_index = 1
	destroy_on_finish=true
