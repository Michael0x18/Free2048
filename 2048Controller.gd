extends Node2D

#Leaderboard
#Inspector mode (manually edit tiles)
#Tile in animation
#Undo/redo

const directory_path = "user://"

var tiles :Array = [[]]
var dupes :Array = []
var score :int = 0
var highscore :int = 0

var sig_save = false
var sig_load = false
var sig_init = false

var num_moved

func init_data_dir() -> void:
	var f:Directory = Directory.new()
	f.open(directory_path)
	if(!f.dir_exists(directory_path)):
		print("crap")
		f.make_dir_recursive
	
func write_highscore():
	var f:File = File.new()
	f.open(directory_path+"/highscore", File.WRITE)
	f.store_string(str(highscore)+"\n")
	f.flush()
	f.close()

#Creates a random tile, of size two (or very rarely four -TODO-).
#Returns true if it can't place a tile
func create_tile_random() -> bool:
	var open_spots:Array = []
	#Iterate by row
	for i in range(4):
		#Iterate by col
		for j in range(4):
			if(tiles[i][j]==null):
				var tmp :Array = [j,i]
				open_spots.append(tmp)
				
	#print(len(open_spots))
	
	if(len(open_spots)==0):
		return true
	
	var scene = load("res://Tile.tscn")
	
	var tile:Tile = scene.instantiate()
	tile.set_name("Tile")
	tile.val = 2
	tile.is_new = true
	tile.update_tile()
	var spot = open_spots[int(randf()*len(open_spots))]
	#print(spot)
	tile.xindex = spot[0]
	tile.yindex = spot[1]
	tiles[tile.yindex][tile.xindex]=tile
	tile.visible = false
	$Game/Tiles.add_child(tile)
	tile.xtarget = itoc(tile.xindex)
	tile.ytarget = itoc(tile.yindex)
	tile.global_position.x = itoc(tile.xindex)
	tile.global_position.y = itoc(tile.yindex)
	tile.visible = true
	
	return false

#Reloads the highscore from the high score file. Returns is_error
func refresh_highscore() -> bool:
	highscore = 0
	var file = File.new()
	file.open(directory_path+"/highscore", File.READ)
	print("Attempting to open: "+directory_path+"/highscore")
	if(!file.is_open()):
		print("Error opening config file, skipping. Highscore will be zero.")
		file.close()
		return true
	var new_highscore = str(file.get_as_text())
	if(new_highscore == null):
		print("Error reading config file, skipping. Highscore will be zero.")
		file.close()
		return true
	highscore = new_highscore.to_int()
	$Game/HighScoreLabel.text = "High Score: "+str(highscore)
	file.close()
	return false
	
func state_save(filename) -> bool:
	var file = File.new()
	file.open(directory_path+"/"+filename, File.WRITE)
	for i in range(4):
		for j in range(4):
			if(tiles[i][j]==null):
				file.store_string("0\n")
			else:
				file.store_string(str(tiles[i][j].val)+"\n")
	file.flush()
	file.close()
	return false
	
func state_load(filename) -> bool:
	subinit()
	var f = File.new()
	f.open(directory_path+"/"+filename, File.READ)
	for i in range(4):
		for j in range(4):
			var v:int = f.get_line().to_int()
			if(v==0):
				continue
			var scene = load("res://Tile.tscn")
			var tile:Tile = scene.instantiate()
			tile.set_name("Tile")
			tile.val = v
			tile.is_new = true
			tile.update_tile()
			tile.xindex = j
			tile.yindex = i
			tiles[i][j] = tile
			$Game/Tiles.add_child(tile)
			tile.xtarget = itoc(tile.xindex)
			tile.ytarget = itoc(tile.yindex)
			tile.global_position.x = itoc(tile.xindex)
			tile.global_position.y = itoc(tile.yindex)
	return false

#Computes absolute coordinates from array index
func itoc(index :int) -> int:
	return index*(128+4)+4+64
	
func subinit() ->void:
	init_data_dir()
	for i in range(len(tiles)):
		for j in range(len(tiles[i])):
			if(tiles[i][j]!=null):
				tiles[i][j].trigger_destroy()
	tiles.clear()
	var blank_arr :Array = [null,null,null,null]
	for i in range(4):
		tiles.append(blank_arr.duplicate(false))
	score = 0
	dupes.clear()
	num_moved = 0
	refresh_highscore()

#(re)initializes game state to the beginning
func init() -> void:
	$Menu.visible=false
	$Game.visible=true
	$AboutMenu.visible=false
	$SandboxMenu.visible=false
	subinit()
	create_tile_random()
	create_tile_random()
	
	

func _ready():
	init()

#Clear tile flags
func cf():
	for i in range(4):
		for j in range(4):
			if(tiles[i][j]!=null):
				tiles[i][j].is_new = false
				
func merge_dupes():
	for tile in dupes:
		tiles[tile.yindex][tile.xindex].val*=2
		tiles[tile.yindex][tile.xindex].update_tile()
		score+=tiles[tile.yindex][tile.xindex].val
		tile.trigger_destroy()
	dupes.clear()

func slide_up():
	cf()
	num_moved = 0
	#Scan from top to bottom in column major
	for col in range(4):
		for row in range(4):
			if(tiles[row][col]!=null):
				#We have encountered a tile that we need to move
				#print(row)
				var last_good_spot = row
				#default to not moving it at all
				for tile_up in range(1,row+1):
					#Move it a maximum of tow tiles up
					var new_pos_test = row-tile_up
					#Find the new testing position
					if(tiles[new_pos_test][col]==null):
						#Position is empty, good to move
						last_good_spot = new_pos_test
					elif(tiles[new_pos_test][col].is_new == false && tiles[new_pos_test][col].val == tiles[row][col].val):
						#This spot is good, but we need to double it up
						last_good_spot = new_pos_test
						#since we hit this, we need to stop looking
						break
					else:
						break
				#At this point, last_good_spot contains what we want
				if(last_good_spot != row):
					#We need to actually go and move it
					if(tiles[last_good_spot][col]!=null):
						#This is tricky, we need to merge them. To do this, we'll remove it from the
						#array altogether and add it to the dupes one. Then, we'll sort through that and
						#merge the tiles in it.
						#print("MERGING")
						dupes.append(tiles[row][col])
						var tmp = tiles[row][col]
						tmp.xindex = col
						tmp.yindex = last_good_spot
						tmp.xtarget = itoc(col)
						tmp.ytarget = itoc(last_good_spot)
						tiles[row][col]=null
						tiles[last_good_spot][col].is_new=true
						num_moved+=1
					if(tiles[last_good_spot][col]==null):
						#print("sliding")
						#EZ, just slide it up
						#print(tiles[row][col])
						tiles[last_good_spot][col]=tiles[row][col]
						#print(tiles[last_good_spot][col])
						tiles[row][col]=null
						tiles[last_good_spot][col].xindex = col
						tiles[last_good_spot][col].yindex = last_good_spot
						tiles[last_good_spot][col].xtarget = itoc(col)
						tiles[last_good_spot][col].ytarget = itoc(last_good_spot)
						num_moved+=1
						#print(tiles[last_good_spot][col])
					#standard moving, this happens every time regardless of the execution pathway
					#print(last_good_spot)
					
		

func slide_down():
	cf()
	num_moved = 0
	for col in range(4):
		for row in range(3,-1,-1):
			if(tiles[row][col]!=null):
				#We have encountered a tile that we need to move
				var last_good_spot = row
				#default to not moving it at all
				for tile_down in range(1,4-row):
					var new_pos_test = row+tile_down
					if(tiles[new_pos_test][col]==null):
						last_good_spot = new_pos_test
					elif(tiles[new_pos_test][col].is_new == false && tiles[new_pos_test][col].val == tiles[row][col].val):
						#This spot is good, but we need to double it up
						last_good_spot = new_pos_test
						#since we hit this, we need to stop looking
						break
					else:
						break
				if(last_good_spot != row):
					#We need to actually go and move it
					if(tiles[last_good_spot][col]!=null):
						#This is tricky, we need to merge them. To do this, we'll remove it from the
						#array altogether and add it to the dupes one. Then, we'll sort through that and
						#merge the tiles in it.
						#print("MERGING")
						dupes.append(tiles[row][col])
						var tmp = tiles[row][col]
						tmp.xindex = col
						tmp.yindex = last_good_spot
						tmp.xtarget = itoc(col)
						tmp.ytarget = itoc(last_good_spot)
						tiles[row][col]=null
						tiles[last_good_spot][col].is_new=true
						num_moved+=1
					if(tiles[last_good_spot][col]==null):
						#print("sliding")
						#EZ, just slide it up
						#print(tiles[row][col])
						tiles[last_good_spot][col]=tiles[row][col]
						#print(tiles[last_good_spot][col])
						tiles[row][col]=null
						tiles[last_good_spot][col].xindex = col
						tiles[last_good_spot][col].yindex = last_good_spot
						tiles[last_good_spot][col].xtarget = itoc(col)
						tiles[last_good_spot][col].ytarget = itoc(last_good_spot)
						num_moved+=1
						#print(tiles[last_good_spot][col])
					#standard moving, this happens every time regardless of the execution pathway
					#print(last_good_spot)



func slide_left():
	cf()
	#This time, we need to process in row major mode, scanning from left to right
	for row in range(4):
		for col in range(4):
			if(tiles[row][col]!=null):
				#We have encountered a tile that we need to move
				var last_good_spot = col
				for tile_left in range(1,col+1):
					var new_pos_test = col-tile_left
					if(tiles[row][new_pos_test]==null):
						#Position is empty, good to move
						last_good_spot = new_pos_test
					elif(tiles[row][new_pos_test].is_new == false && tiles[row][new_pos_test].val == tiles[row][col].val):
						#This spot is good, but we need to double it up
						last_good_spot = new_pos_test
						#since we hit this, we need to stop looking
						break
					else:
						break
				if(last_good_spot != col):
					#We need to actually go and move it
					if(tiles[row][last_good_spot]!=null):
						#This is tricky, we need to merge them. To do this, we'll remove it from the
						#array altogether and add it to the dupes one. Then, we'll sort through that and
						#merge the tiles in it.
						#print("MERGING")
						dupes.append(tiles[row][col])
						var tmp = tiles[row][col]
						tmp.xindex = last_good_spot
						tmp.yindex = row
						tmp.xtarget = itoc(last_good_spot)
						tmp.ytarget = itoc(row)
						tiles[row][col]=null
						tiles[row][last_good_spot].is_new=true
						num_moved+=1
					if(tiles[row][last_good_spot]==null):
						#print("sliding")
						#EZ, just slide it up
						#print(tiles[row][col])
						tiles[row][last_good_spot]=tiles[row][col]
						#print(tiles[last_good_spot][col])
						tiles[row][col]=null
						tiles[row][last_good_spot].xindex = last_good_spot
						tiles[row][last_good_spot].yindex = row
						tiles[row][last_good_spot].xtarget = itoc(last_good_spot)
						tiles[row][last_good_spot].ytarget = itoc(row)
						num_moved+=1
						#print(tiles[last_good_spot][col])
					#standard moving, this happens every time regardless of the execution pathway
					#print(last_good_spot)
					

func slide_right():
	cf()
	#This time, we need to process in row major mode, scanning from right to left
	for row in range(4):
		for col in range(3,-1,-1):
			if(tiles[row][col]!=null):
				#We have encountered a tile that we need to move
				var last_good_spot = col
				for tile_left in range(1,4-col):
					var new_pos_test = col+tile_left
					if(tiles[row][new_pos_test]==null):
						#Position is empty, good to move
						last_good_spot = new_pos_test
						#print("Moving to empty space")
					elif(tiles[row][new_pos_test].is_new == false && tiles[row][new_pos_test].val == tiles[row][col].val):
						#This spot is good, but we need to double it up
						last_good_spot = new_pos_test
						#print("Hit a merger")
						#since we hit this, we need to stop looking
						break
					else:
						#print("Hit a blocker")
						break
				if(last_good_spot != col):
					#We need to actually go and move it
					if(tiles[row][last_good_spot]!=null):
						#This is tricky, we need to merge them. To do this, we'll remove it from the
						#array altogether and add it to the dupes one. Then, we'll sort through that and
						#merge the tiles in it.
						#print("MERGING")
						dupes.append(tiles[row][col])
						var tmp = tiles[row][col]
						tmp.xindex = last_good_spot
						tmp.yindex = row
						tmp.xtarget = itoc(last_good_spot)
						tmp.ytarget = itoc(row)
						tiles[row][col]=null
						tiles[row][last_good_spot].is_new=true
						num_moved+=1
					if(tiles[row][last_good_spot]==null):
						#print("sliding")
						#EZ, just slide it up
						#print(tiles[row][col])
						tiles[row][last_good_spot]=tiles[row][col]
						#print(tiles[last_good_spot][col])
						tiles[row][col]=null
						tiles[row][last_good_spot].xindex = last_good_spot
						tiles[row][last_good_spot].yindex = row
						tiles[row][last_good_spot].xtarget = itoc(last_good_spot)
						tiles[row][last_good_spot].ytarget = itoc(row)
						num_moved+=1
						#print(tiles[last_good_spot][col])
					#standard moving, this happens every time regardless of the execution pathway
					#print(last_good_spot)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#print(tiles)
	var finished_moving := true
	for i in range(4):
		for j in range(4):
			if(tiles[i][j]!=null && (tiles[i][j].global_position.x != tiles[i][j].xtarget || tiles[i][j].global_position.y != tiles[i][j].ytarget)):
				finished_moving = false
	if(sig_save):
		if(!finished_moving):
			return
		else:
			state_save("2048state")
			sig_save = false
	if(sig_load):
		if(!finished_moving):
			return
		else:
			state_load("2048state")
			sig_load = false
	if(sig_init):
		if(!finished_moving):
			return
		else:
			init()
			sig_init = false
	
	if(finished_moving):
		if(len(dupes)>0):
			merge_dupes()
			return
		if(num_moved>0):
			num_moved = 0
			create_tile_random()
		var dir = -1
		if(Input.is_action_just_pressed("ui_up")):
			dir=0
			slide_up()
		elif(Input.is_action_just_pressed("ui_right")):
			dir=1
			slide_right()
		elif(Input.is_action_just_pressed("ui_down")):
			dir=2
			slide_down()
		elif(Input.is_action_just_pressed("ui_left")):
			dir=3
			slide_left()
		elif(Input.is_action_just_pressed("ui_cancel")):
			create_tile_random()
	#update score labels
	$Game/ScoreLabel.text = "Score: "+str(score)
	#print(str(score)+" "+str(highscore))
	#print(int(score))
	#print(int(highscore))
	#print(int(score)>int(highscore))
	if(score>highscore):
		#print("REE")
		highscore=score
		$Game/HighScoreLabel.text = "High Score: "+str(highscore)
		write_highscore()


func _on_save_button_pressed():
	sig_save = true


func _on_load_button_pressed():
	sig_load = true


func _on_reset_button_pressed():
	sig_init = true


func _on_menu_button_pressed():
	$Menu.visible=true
	$Game.visible=false
	$SandboxMenu.visible=false
	$AboutMenu.visible=false


func _on_exit_return_button_pressed():
	$Menu.visible=false
	$Game.visible=true
	$SandboxMenu.visible=false
	$AboutMenu.visible=false


func _on_about_button_pressed():
	$Menu.visible=false
	$Game.visible=false
	$SandboxMenu.visible=false
	$AboutMenu.visible=true
	
func _on_sandbox_button_pressed():
	$Menu.visible=false
	$Game.visible=false
	$SandboxMenu.visible=true
	$AboutMenu.visible=false


func _on_sandbox_reset_button_pressed():
	for n in $SandboxMenu/TileContainer.get_children():
		n.text = ""


func _on_sandbox_apply_button_pressed():
	var nodes = $SandboxMenu/TileContainer.get_children()
	subinit()
	for i in range(4):
		for j in range(4):
			if(nodes[i*4+j].text==""):
				continue
			var v:int = nodes[i*4+j].text.to_int()
			var scene = load("res://Tile.tscn")
			var tile:Tile = scene.instantiate()
			tile.set_name("Tile")
			tile.val = v
			tile.is_new = true
			tile.update_tile()
			tile.xindex = j
			tile.yindex = i
			tiles[i][j] = tile
			$Game/Tiles.add_child(tile)
			tile.xtarget = itoc(tile.xindex)
			tile.ytarget = itoc(tile.yindex)
			tile.global_position.x = itoc(tile.xindex)
			tile.global_position.y = itoc(tile.yindex)
	_on_exit_return_button_pressed()
